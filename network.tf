data "aws_vpc" "main" {
  id = var.vpcid
}

data "aws_subnet" "main"{
  id = "subnet-ee3a7a98"
}

resource "aws_security_group" "Packer" {
  name        = "Packer"
  description = "Allow TLS inbound traffic"
  vpc_id     = data.aws_vpc.main.id
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
  # WinRM access from anywhere
   ingress {
     from_port   = 5985
     to_port     = 5986
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }

     # RDP access from anywhere
   ingress {
     from_port   = 3389
     to_port     = 3389
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }

   ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
        from_port = 0 
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
   }
  tags = {
    Name = "Packer"
  }
}
