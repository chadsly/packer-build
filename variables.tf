variable "aws_region"{
  default = "us-gov-west-1"
}

variable "AWS_ACCESS_KEY"{
  default = ""
}

variable "AWS_SECRET_KEY"{
  default = ""
}

variable "aws_ami"{
  default="ami-88d08be9"
  #"ami-43eab222"
}

variable "vpcid" {
  default="vpc-1fce497b"
}

variable "JASON_KEY" {
  default=""
}