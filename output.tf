output "region" {
  value = var.aws_region
}

output "metal_ip" {
  value = aws_instance.Packer-build-server.public_dns
}

output "rendered" {
  value = templatefile("hosts_template.tpl", { ip_addrs = aws_instance.Packer-build-server.*.public_dns})
}
