terraform {
backend "s3" {
  bucket = "cie-tfstate"
  key    = "gitlab/terraform.tfstate"
  region = "us-gov-west-1"
}
}