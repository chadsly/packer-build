# Log into AWS
provider "aws" {
  #version = "~> 2.0"
  region = var.aws_region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}


resource "aws_instance" "Packer-build-server" {
  ami           = var.aws_ami
  instance_type = "c5n.metal"
  subnet_id = data.aws_subnet.main.id
  security_groups = [aws_security_group.Packer.id]
  key_name = "jason-key"
  #get_password_data = true
  tags = {
    Name  = "Machine-Image-builder"
  }
}
